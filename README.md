# Modular Laravel Structure #

Implement modular on laravel.

## Setup ##
composer require glacialblade/modular

### Add to providers on app.php ###
Glacialblade\Modular\ModularServiceProvider::class

### Generate Config File ###
php artisan modular:config