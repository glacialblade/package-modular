<?php namespace Glacialblade\Modular;

use Glacialblade\Modular\Console\Commands\ModularConfigCommand;
use Glacialblade\Modular\Console\Commands\ModularModuleCommand;
use Illuminate\Support\ServiceProvider;

class ModularServiceProvider extends ServiceProvider {

	public function boot() {
		$modules = config('modular.modules');
		$path    = config('modular.path');

		if($modules) {
			foreach($modules as $d=>$module) {
				while(list(, $m) = each($module)) {
					// Include the routes.
					$routesPath = $path.'/'.$d.'/'.$m.'/routes.php';
					if(file_exists($routesPath)) {
						include $routesPath;
					}

					// Include the views
					$viewsPath = $path.'/'.$d.'/'.$m.'/Views';
					if(is_dir($viewsPath)) { 
						$this->loadViewsFrom($viewsPath, $d.".".$m);
					}
				}
			}
		}
	}

	public function register() {
		$this->registerCommands();
	}

	protected function registerCommands() {
		$this->commands([
			ModularConfigCommand::class,
			ModularModuleCommand::class
		]);
	}
}