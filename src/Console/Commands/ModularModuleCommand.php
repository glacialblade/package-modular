<?php

namespace Glacialblade\Modular\Console\Commands;

use Illuminate\Console\Command;

class ModularModuleCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'modular:module {location} {module}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate new module.';


	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire() {
		$location = $this->argument('location');
		$module   = $this->argument('module');

		// Create Modules Folder
		$modulePath = base_path().'/app/Modules/';
		if(!file_exists($modulePath)) {
			mkdir($modulePath);
		}
		$locationPath = $modulePath.$location.'/';
		if(!file_exists($locationPath)) {
			mkdir($locationPath);
		}

		// Create the New Module
		$newModulePath = $locationPath.'/'.$module;
		if(!file_exists($newModulePath)) {
			mkdir($newModulePath, 0755);
			mkdir($newModulePath.'/Controllers', 0755);
			mkdir($newModulePath.'/Views', 0755);

			$this->createController($module, $newModulePath.'/Controllers/', $location);
			$this->createRoute($module, $newModulePath.'/', $location);
		}
		else {
			$this->info($module.' Module already exists.');
		}
	}

	/**
	 * Create Controller File
	 * @param  $controller
	 * @param  $path
	 * @param  $location
	 */
	public function createController($controller, $path, $location) {
		$file = fopen("$path/${controller}Controller.php", 'w');
		$className = $controller.'Controller';

        $template = <<<EOF
<?php namespace App\Modules\\$location\\$controller\Controllers;

class $className extends \App\Http\Controller {

	public function index() {

	}

}
EOF;

        fwrite($file, $template);
	}

	/**
	 * Create Routes File
	 * @param  $controller
	 * @param  $path
	 * @param  $location
	 */
	public function createRoute($controller, $path, $location) {
		$file = fopen("$path/routes.php", 'w');

        $template = <<<EOF
<?php
\$controllerPath = "App\Modules\\$location\\$controller\Controllers\\\\";
EOF;

        fwrite($file, $template);
	}
}