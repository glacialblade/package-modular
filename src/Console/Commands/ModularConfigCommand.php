<?php

namespace Glacialblade\Modular\Console\Commands;

use Illuminate\Console\Command;

class ModularConfigCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'modular:config';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate config file.';


	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire() {
		$source      = __DIR__.'/../../Config/modular.php';
		$destination =  config_path('modular.php');

		if(!file_exists($destination)) {
			copy($source, $destination);
		}
		else {
			$this->info('modular config already exists.');
		}
	}
}