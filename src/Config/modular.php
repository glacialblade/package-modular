<?php

return [
	'path' => base_path().'/app/Modules',

	/**
	 * Modules
	 */
	'modules' => [
		'Admin' => [
			'Auth'
		],

		'Guest' => [
			'Home'
		]
	]
];